from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from markdownx import utils

register = template.Library()


@register.filter(needs_autoescape=True)
@stringfilter
def markdownify(v, autoescape=True):
    esc = conditional_escape if autoescape else (lambda x: x)
    result = utils.markdownify(esc(v))
    return mark_safe(result)
