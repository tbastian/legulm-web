from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView


class HomeView(TemplateView):
    ''' Home page '''
    template_name = 'mainsite/homepage.html'


class RulesView(TemplateView):
    ''' Legulm's rules '''
    template_name = 'mainsite/rules.html'

    def post(self, request):
        ''' Handle rules' acceptance '''
        data = request.POST
        if request.user.is_authenticated:
            if 'accept' in data and data['accept'] == '1':
                legulm_user = request.user.legulmuser
                legulm_user.has_accepted_conditions = True
                legulm_user.save()

        return self.get(request)
