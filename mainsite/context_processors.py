from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext as _
from django.urls import reverse


def legulm_user(req):
    ''' Add the legulm_user matching the request's user (if it exists) to the
    context '''

    extra_context = {}

    extra_context['legulm_user'] = None
    if req.user.is_authenticated:
        try:
            extra_context['legulm_user'] = req.user.legulmuser
        except ObjectDoesNotExist:
            pass

    return extra_context


def aggregate_lasting_messages(req):
    ''' If needed, add lasting messages to be displayed '''

    messages = []

    # Accepted conditions
    if req.user.is_authenticated:
        try:
            user = req.user.legulmuser
            if not user.has_accepted_conditions:
                messages.append(_(
                    "To be a member of Légulm and buy baskets, you must first "
                    "[read and acknowledge Légulm's rules]({}).")
                                .format(reverse('legulm_rules')))
        except ObjectDoesNotExist:
            pass

    extra_context = {
        "lasting_messages": messages,
    }
    return extra_context
