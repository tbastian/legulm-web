""" Signal receivers for mainsite """

from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

from .models import LegulmUser


@receiver(user_signed_up)
def upon_account_creation(sender, request, user, **kwargs):
    ''' Triggered upon user creation. Handles creating a LegulmUser profile,
    etc. '''

    try:
        legulm_user = user.legulmuser
    except ObjectDoesNotExist:
        # No matching LegulmUser yet
        legulm_user = LegulmUser(user=user,
                                 has_accepted_conditions=False)
        legulm_user.save()
