from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.translation import gettext as _

from markdownx.models import MarkdownxField

from solo.models import SingletonModel


class LegulmUser(models.Model):
    """ A user, with more data stored """

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True)

    has_accepted_conditions = models.BooleanField(
        verbose_name=_("Accepted rules"))

    def upcoming_baskets(self):
        """ Get the list of upcoming baskets for this user """

        regs = DeliveryRegistration.objects.filter(
            user=self,
            delivery__date__gte=timezone.now().date())
        return regs

    def upcoming_basket_dates(self):
        """ Get the ready-to-display list of upcoming baskets for this user """
        regs = self.upcoming_baskets()
        upcoming = []
        for reg in regs:
            entry = {
                'date': reg.delivery.date,
                'number': reg.nb_baskets,
            }
            upcoming.append(entry)
        return upcoming

    def __str__(self):
        return self.user.username


class Delivery(models.Model):
    """ A delivery day, with its basket """

    date = models.DateField(
        verbose_name=_("Date"))


class Basket(models.Model):
    """ An AMAP basket """

    delivery = models.ForeignKey(
        Delivery,
        on_delete=models.CASCADE)

    # TODO basket contents


class DeliveryRegistration(models.Model):
    """ A registration of a user for some delivery """

    delivery = models.ForeignKey(
        Delivery,
        on_delete=models.CASCADE)
    user = models.ForeignKey(
        LegulmUser,
        on_delete=models.CASCADE)
    nb_baskets = models.IntegerField(
        verbose_name=_("Number of baskets"),
        default=1)


class SiteConfiguration(SingletonModel):
    ''' This website's configuration, as a solo model '''

    legulm_rules = MarkdownxField(
        help_text=_("This is a Markdown field"),
        verbose_name=_("Légulm's rules, which must be acknowledged by users"),
    )

    def __str__(self):
        return _("Website configuration")

    class Meta:
        verbose_name = _("Website configuration")
