from django.apps import AppConfig


class MainsiteConfig(AppConfig):
    name = 'mainsite'

    def ready(self):
        from . import signals
