from django.contrib import admin

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from solo.admin import SingletonModelAdmin

from . import models


class LegulmUserInline(admin.StackedInline):
    ''' Inline legulm user augmentation '''
    model = models.LegulmUser
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (LegulmUserInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(models.Delivery)
admin.site.register(models.Basket)
admin.site.register(models.DeliveryRegistration)

admin.site.register(models.SiteConfiguration, SingletonModelAdmin)
