from django.urls import path, include

from .views import recipes_blog_views, basket_content_blog_views


urlpatterns = [
    path('paniers/', include(basket_content_blog_views.urls())),
    path('recettes/', include(recipes_blog_views.urls())),
]
