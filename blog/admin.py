from django.contrib import admin
from . import models


@admin.register(models.RecipeBlogPost, models.BasketContentBlogPost)
class BlogAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
