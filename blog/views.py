from django.utils.translation import gettext as _
from django.shortcuts import render, get_object_or_404
from django.urls import path
from django.views.generic import ListView
from .models import RecipeBlogPost, BasketContentBlogPost


class BlogViews:
    """ Handles a single blog, with its attributes and related models. """

    def __init__(self, post_model, name, title, posts_per_page=24):
        self.post_model = post_model
        self.name = name
        self.title = title
        self.posts_per_page = posts_per_page

        self.post_model.view_name = self.post_details_name()
        self._init_classes()

    def _init_classes(self):
        """ Declares instance-local classes, needing to have class-variables
        which depends on the instance of BlogViews in which we are. """

        parent = self

        class PostList(ListView):
            """ Display a list of blog entries. """
            nonlocal parent

            paginate_by = parent.posts_per_page
            model = parent.post_model
            populate_context = parent.populate_context
            template_name = 'blog/post_list.html'

            def get_context_data(self, **kwargs):
                context = self.populate_context(
                    super().get_context_data(**kwargs))
                return context

        self.post_list = PostList.as_view()

    def populate_context(self, context):
        """ Append a few commonly-shared entries to a rendering context """
        common_context = {
            'blog_name': self.name,
            'blog_title': self.title,
            'post_details_name': self.post_details_name(),
        }
        common_context.update(context)
        return common_context

    def post_list_name(self):
        """ View name of `post_list` is in the url resolver """
        return '{}-list'.format(self.name)

    def post_details_name(self):
        """ View name of `post_details` is in the url resolver """
        return '{}-post'.format(self.name)

    def post_details(self, req, year, month, day, slug):
        """ Display a whole post """

        post = get_object_or_404(
            self.post_model,
            slug=slug,
            date__year=year,
            date__month=month,
            date__day=day,
        )

        context = self.populate_context({
            'post': post,
        })
        return render(req, 'blog/post_details.html', context=context)

    def urls(self):
        return [
            path('<int:year>/<int:month>/<int:day>/<slug:slug>',
                 self.post_details,
                 name=self.post_details_name()),
            path('',
                 self.post_list,
                 name=self.post_list_name())
        ]


recipes_blog_views = BlogViews(RecipeBlogPost, 'recipes', _("Recipes"))
basket_content_blog_views = BlogViews(BasketContentBlogPost,
                                      'basket_content',
                                      _("Basket contents"))
