from django.utils.translation import gettext as _
from django.db import models
from . import abstract_models


class RecipeBlogPost(abstract_models.BlogPostAbstract):
    """ Blog posts for the recipes blog """

    class Meta:
        verbose_name = _("Recipe post")



class BasketContentBlogPost(abstract_models.BlogPostAbstract):
    """ Blog posts for the basket contents blog """

    class Meta:
        verbose_name = _("Week's basket contents")

        # Translators: this is the plural form
        verbose_name_plural = _("Week's basket contents")
