from django.utils.translation import gettext as _
from django.db import models
from django.conf import settings
from django.urls import reverse


class BlogPostAbstract(models.Model):
    """ An abstract model for a blog post. """

    title = models.CharField(_("Title"),
                             max_length=255)
    date = models.DateTimeField(_("Date"))
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.SET_NULL,
                               null=True)
    content = models.TextField(_("Content"))  # TODO update to markdownx
    slug = models.SlugField(_("Address"))

    def get_url(self):
        kwargs = {
            'year': self.date.year,
            'month': self.date.month,
            'day': self.date.day,
            'slug': self.slug,
        }
        return reverse(self.view_name,
                       kwargs=kwargs)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True
        ordering = ['-date']
